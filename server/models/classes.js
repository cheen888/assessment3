// model definition of 'classes' table in 'fitly' database
module.exports = function(database, Sequelize) {
    // 'classes' is the same name as the SQL database
    var Class = database.define('classes', {
        // map everything here just like it is on SQL 'classes'
            kid: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            title: {
                type: Sequelize.STRING(64),
                allowNull: false,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            activity_code: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            description: {
                type: Sequelize.STRING(255),
                allowNull: true,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            creator_id: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            status: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false
            },
            deletedAt: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }, {
            // 'classes' is the table name on the 'fitly' database
            tableName: 'classes',
            // Allow timestamp attributes (updatedAt, createdAt)
            // By default, added to know when db entry added & last updated
            timestamps: true,
            paranoid: true
        }
    );
    return Class;
};