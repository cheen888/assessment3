// model definition of 'activities' table in 'fitly' database
module.exports = function(database, Sequelize) {
    // 'activities' is the same name as the SQL database
    var Activity = database.define('activities', {
        // map everything here just like it is on SQL 'activities'
            a_code: {
                type: Sequelize.STRING(5),
                primaryKey: true,
                allowNull: false
            },
            a_title: {
                type: Sequelize.STRING(64),
                allowNull: false,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            }
        }, {
            freezeTableName: true,
            // 'activities' is the table name on the 'fitly' database
            tableName: 'activities',
            // Allow timestamp attributes (updatedAt, createdAt)
            // By default, added to know when db entry added & last updated
            timestamps: false
        }
    );
    return Activity;
};