### For Assessment 3
# Project Fitly
### Connecting health-conscious consumers to fitness providers
An NUS-ISS Full Stack Foundation course project work-in-progress

##Project members
1. Nick Sabai nicksabai@gmail.com
2. Tan Cheen Chong cheen@genii-group.com

## ID and password for Admin access to system
Email: q@q
Password: 1234

## Capabilities that worked
* Database
    * Three (3) MySQL tables (users, userqualify, qualifications)
    * Table 'users' has
        * uid (INT) column as primary key and set to auto-increment
        * CreatedAt, Updated and DeletedAt date fields with sequelize model setting set to 'paranoid' to enable soft-delete
    * Table 'userqualify' links 'users' to 'qualifications'
        * uses foreign keys to JOIN both tables
        * Purpose: a user can have more than one qualifications
* APIs
    * Routes with endpoints defined for CRUD actions
        * Add a user             POST /api/users
        * Get a user details     GET /api/users/:userId
        * Edit/update a user     PUT /api/users/:userId
        * Delete a user          DELETE /api/users/:userId
        * Get a list of users    GET /api/users
        * Sign in a user (local) POST /api/users/auth
        * Sign out a user        GET /signout
    * Returned appropriate response codes for HTTP request
        * 200 for success server action
        * 201 for resource (e.g. user) created
        * 404 if resource (e.g. user) is not found
        * 409 when request to add a user whose email already exists in db
        * 500 if server/db encounter an error 
* Frontend
    * Login Page
    * Logout Function
    * List of Users in db
    * Create users (as Admin)
    * View any user profile (as Admin)
    * Edit user (as Admin)
    * Delete user (as Admin)

## Capabilities requiring more effort
* Create a user and updating multiple tables (e.g. adding multiple qualifications per user) using sequelizer transactions
* Login bug. While authenticating engine recognises valid/invalid emails and valid/invalid passwords, and return 'done' correctly, user still gets routed to 'user list' state
* We didn't have enough bandwidth to perform validation, both at the browser and server-level

## New lessons and insights
* Back-end
    * It is amazing how any app idea boils down to basic CRUD execution at the back-end
    * Getting endpoints and CRUD operations planned early, and coded at the server-side right from the start smoothens the integration with front-end
    * POSTMAN is really cool to test server APIs
    * We are humbled by and view the authentication process differently now
        * There are so many steps involved, including serializer and deserializer that we had taken for granted all these years
* Front-end
    * ng-href helps bind a link to other angular actions (e.g. ng-click)
* Cloud deployment
    * Cloud deployment is tedious and full of potential typo errors
    * Our free gitbucket account stopped executing pipeline builds (after exceeding 50 for the month!)
