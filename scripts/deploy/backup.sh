#!/bin/bash
ASSESSMENT3_HOME=$HOME/assessment3-c

if [ -d "$ASSESSMENT3_HOME" ]; then
   zip -r $HOME/backup/$(date +"%m-%d-%Y").zip $ASSESSMENT3_HOME/
fi
rm -rf $ASSESSMENT3_HOME/*
rm -rf $ASSESSMENT3_HOME/.git