CREATE DATABASE if not exists `fitness` ;
USE `fitness`;

-- table to connect client to runs (which is a representation of classes), and trainers
CREATE TABLE if not exists `qualifications`
(
  `q_code` VARCHAR(5) NOT NULL,
  `q_name` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`q_code`)
);

-- table to store admin & trainer profiles
CREATE TABLE if not exists `users` (
  `uid` INT AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` CHAR(64) NOT NULL,
  -- to define roles: Admin vs trainer (vs clients?)
  `role` TINYINT NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `status` TINYINT NOT NULL,
  `createdAt` DATETIME,
  `updatedAt` DATETIME,
  `deletedAt` DATETIME,
  PRIMARY KEY (`uid`),
  UNIQUE INDEX `uid_UNIQUE` (`uid` ASC),
  -- trainer email must also be unique in our system
  UNIQUE INDEX `email_UNIQUE` (`email` ASC)
);

-- table to link user to any qualified fitness activities
-- a trainer can have multiple fitness offerings
CREATE TABLE if not exists `userqualify`
(
  `user_id` INT NOT NULL,
  `qualify_id` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`user_id` , `qualify_id`),
  KEY `qualify_id` (`qualify_id`),
  FOREIGN KEY (`user_id`) REFERENCES `fitness`.`users`(`uid`) ON DELETE CASCADE,
  FOREIGN KEY (`qualify_id`) REFERENCES `fitness`.`qualifications`(`q_code`) ON DELETE CASCADE
);

-- table to store a unique class, which can have multiple repeated schedules
CREATE TABLE if not exists `classes`
(
  `kid` INT AUTO_INCREMENT,
  `title` VARCHAR(64) NOT NULL,
  `description` VARCHAR(255),
  `creator_id` INT NOT NULL,
  `status` TINYINT NOT NULL,
  `createdAt` DATETIME,
  `updatedAt` DATETIME,
  `deletedAt` DATETIME,
  PRIMARY KEY (`kid`),
  UNIQUE INDEX `kid_UNIQUE` (`kid` ASC),
  FOREIGN KEY (`creator_id`) REFERENCES `fitness`.`users`(`uid`)
);

-- table to store runs/repeat of the unique class
CREATE TABLE if not exists `schedules`
(
  `sid` INT AUTO_INCREMENT,
  `kid` INT NOT NULL,
  `uid` INT NOT NULL,
  `datestart` DATETIME NOT NULL,
  `duration` INT NOT NULL,
  `minsize` INT NOT NULL,
  `maxsize` INT NOT NULL,
  `instructions` VARCHAR(255) NOT NULL,
  `status` TINYINT NOT NULL,
  `createdAt` DATETIME,
  `updatedAt` DATETIME,
  `deletedAt` DATETIME,
  PRIMARY KEY (`sid`),
  UNIQUE INDEX `sid_UNIQUE` (`sid` ASC),
  FOREIGN KEY (`kid`) REFERENCES `fitness`.`classes`(`kid`),
  FOREIGN KEY (`uid`) REFERENCES `fitness`.`users`(`uid`)
);